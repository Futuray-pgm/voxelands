/************************************************************************
* Minetest-c55
* Copyright (C) 2010-2011 celeron55, Perttu Ahola <celeron55@gmail.com>
*
* tutorial.h
* voxelands - 3d voxel world sandbox game
* Copyright (C) Lisa 'darkrose' Milne 2013-2014 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
*
* License updated from GPLv2 or later to GPLv3 or later by Lisa Milne
* for Voxelands.
*
* Created by Futuray
************************************************************************/

#include "tutorial.h"

Tutorial::Tutorial():
	currentTutorial(0)
{
};

Tutorial::Tutorial(int current):
	currentTutorial(current)
{
};

Tutorial::~Tutorial()
{
};

std::string Tutorial::getText(int num)
{
	switch(num) // TODO : translations
	{
		case TUTORIAL_WALK :
			return (std::string)"Press the [W] button to walk forward.";
		
		case TUTORIAL_JUMP :
			return (std::string)"Press the spacebar to jump.";

		/* case TUTORIAL_LOOK :
			return (std::string)"Move the mouse to look around you."; */

		case TUTORIAL_INVENTORY :
			return (std::string)"Press the [I] button to open your inventory\nand the [Q] button to interact with nodes.";

		/*case TUTORIAL_DIG_PLACE :
			return (std::string)"Use the left click to dig nodes and the right click to place them.";*/

		case TUTORIAL_EAT :
			return (std::string)"Use the [H] button to eat any food.";

		default :
			return (std::string)"Tutorial is done!";
	};
}

void Tutorial::setCurrentTutorialToNext()
{
	this->currentTutorial++;
}

void Tutorial::setCurrentTutorialToNumber(int number)
{
	this->currentTutorial = number;
}

int Tutorial::getCurrentTutorialNumber()
{
	return this->currentTutorial;
}

int Tutorial::getCurrentTutorialCompletionValue()
{
	switch(this->currentTutorial)
	{
		case TUTORIAL_WALK :
			return TUTORIAL_KEY_WALK;
		
		case TUTORIAL_JUMP :
			return TUTORIAL_KEY_JUMP;

		/* case TUTORIAL_LOOK :
			return (std::string)"Move the mouse to look around you."; */

		case TUTORIAL_INVENTORY :
			return TUTORIAL_KEY_INVENTORY;

		/*case TUTORIAL_DIG_PLACE :
			return (std::string)"Use the left click to dig nodes and the right click to place them.";*/

		case TUTORIAL_EAT :
			return TUTORIAL_KEY_EAT;

		default :
			return 0;
	};
}

std::string Tutorial::getCurrentTutorialText()
{
	return this->getText(this->currentTutorial);
}

std::string Tutorial::getNumberText(int number)
{
	return this->getText(number);
}
