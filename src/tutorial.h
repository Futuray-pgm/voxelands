/************************************************************************
* Minetest-c55
* Copyright (C) 2010-2011 celeron55, Perttu Ahola <celeron55@gmail.com>
*
* tutorial.h
* voxelands - 3d voxel world sandbox game
* Copyright (C) Lisa 'darkrose' Milne 2013-2014 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
*
* License updated from GPLv2 or later to GPLv3 or later by Lisa Milne
* for Voxelands.
*
* Created by Futuray
************************************************************************/

/* 2304
			{
				vlprintf(CN_INFO,"Tuto Begin");
				core::dimension2d<u32> screensize = driver->getScreenSize();
				s32 x = (screensize.Width/2);
				s32 y = (screensize.Height/2);
				LocalPlayer* player = client.getLocalPlayer();
				Tutorial *tuto = player->tutorial;
				std::string tutotext = tuto->getCurrentTutorialText();
				std::wstring text = narrow_to_wide(tutotext);
				core::dimension2d<u32> textsize = guienv->getSkin()->getFont()->getDimension(text.c_str());
				core::rect<s32> rect((x-412)-(textsize.Width/2), y+10, (x-412)+(textsize.Width/2), y+10+textsize.Height);
				guienv->addStaticText(text.c_str(),rect);
				vlprintf(CN_INFO,"Tuto OK"); //chatline_add(&chat_lines,narrow_to_wide(gettext("HUD shown")),-103.00);
			}
*/

// 395 [player.h]
// #include "tutorial.h"
// Tutorial *tutorial;

// 76 [player.cpp]
// tutorial(new Tutorial())

#ifndef TUTORIAL_HEADER
#define TUTORIAL_HEADER

#include "log.h"
#include "common.h"
#include "keycode.h"

#include <string>

#define TUTORIAL_WALK      0
#define TUTORIAL_JUMP      1
// #define TUTORIAL_LOOK      2 # FIXME : find a way to check if the player look around
#define TUTORIAL_INVENTORY 2
// #define TUTORIAL_DIG_PLACE 3 # FIXME : find a way to check if the player push Leftclick
#define TUTORIAL_EAT       3

#define TUTORIAL_KEY_WALK      VLKC_FORWARD
#define TUTORIAL_KEY_JUMP      VLKC_JUMP
// #define TUTORIAL_KEY_LOOK ???
#define TUTORIAL_KEY_INVENTORY VLKC_INVENTORY
// #define TUTORIAL_KEY_DIG_PLACE VLKC_FORWARD
#define TUTORIAL_KEY_EAT       VLKC_USE

class Tutorial
{
public:
	
	Tutorial();
	Tutorial(int current);
	virtual ~Tutorial();

	std::string getText(int num);
	void setCurrentTutorialToNext();
	void setCurrentTutorialToNumber(int number);
	int getCurrentTutorialNumber();
	int getCurrentTutorialCompletionValue();
	std::string getCurrentTutorialText();
	std::string getNumberText(int number);

	int currentTutorial;
};
#endif
