# About

**Voxelands** is a sandbox construction game based on Minetest 0.3, which was inspired by earlier "voxel world" games such as Infiniminer. Gameplay puts players in a fully destructible 3D game world where they can place and dig numerous types of blocks, items and creatures using a variety of tools. Inside the game world players can build structures, artworks and anything else their creativity can think of on multiplayer servers and singleplayer worlds across multiple game modes.

# Chats

Matrix: <https://matrix.to/#/#voxelands-room:matrix.org>

# Dependencies

Voxelands requires:
* the standard C++ library
* irrlicht
* freetype
* openAL
* vorbisfile
* ogg
* zlib
* cmake
* make
* g++ (or other C++ compiler)
* bzip2
* jpeg
* libxml2

# Install

## Download

Install dependencies. Here's an example for Debian/Ubuntu:

    sudo apt install build-essential libirrlicht-dev cmake libbz2-dev zlib1g-dev libfreetype6-dev libpng-dev libjpeg-dev libogg-dev libvorbis-dev libopenal-dev libxxf86vm-dev libgl1-mesa-dev libsqlite3-dev libxml2-dev

You can install Git for easily keeping your copy up-to-date.
If you don’t want Git, read below on how to get the source without Git.

Download source (this is the URL to the latest of source repository, which might not work at all times) using Git:

    git clone https://git.minetest.land/oerkki/voxelands.git
    cd voxelands

Download source, without using Git:

    wget https://git.minetest.land/oerkki/voxelands/archive/master.tar.gz
    tar xf master.tar.gz
    cd voxelands

## Build

Build a version that runs directly from the source directory:

    cmake . -DRUN_IN_PLACE=TRUE
    make -j$(nproc)

- Use `cmake . -LH` to see all CMake options and their current state.
- If you want to install it system-wide (or are making a distribution package),
  you will want to use `-DRUN_IN_PLACE=FALSE`.
- You can build a bare server by specifying `-DBUILD_SERVER=TRUE`.
- You can disable the client build by specifying `-DBUILD_CLIENT=FALSE`.
- You can select between Release and Debug build by `-DCMAKE_BUILD_TYPE=<Debug or Release>`.
  - Debug build is slower, but gives much more useful output in a debugger.

### Errors

Some buildings errors are currently known. If the building occured an error, it might be one of them:

#### Crash when building with translation.

**Error :**

    [  2%] Linking CXX static library libcguittfont.a
    [  2%] Built target cguittfont
    make: *** [Makefile:152 : all] Erreur 2

**Git issue:** [#19](https://git.minetest.land/oerkki/voxelands/issues/19)

**Possible fixes:**

1. Change your default build language to english: (Add the `sudo` keyword for ubuntu-based devices)
    apt-get install language-pack-en
    dpkg-reconfigure locales

2. Delete translations files by simply deleting the `po` folder.

#### The `-DRUN_IN_PLACE` CMake option doesn't work.

**Git issue:** [#19](https://git.minetest.land/oerkki/voxelands/issues/11)

This error is fixed since the version 2404 of Voxelands. If you get them, check if the dowloaded version is the good one.

# Use

Simply run `./bin/voxelands` from any console/terminal.

# Controls

Controls can be set in the Settings tab of the main menu

| Button             | Action                                  |
| ------------------ | --------------------------------------- |
| W, S, A, D         | Walk forward, backward, left, and right |
| Mouse              | Look / Aim                              |
| Mouse wheel        | Change wielded item                     |
| 1 through 8        | Change wielded item                     |
| Right Mouse Button | Place item                              |
| Left Mouse Button  | Dig / Punch / Attack                    |
| Shift              | Sneak                                   |
| Space              | Jump                                    |
| R                  | Move Up (ladders, swimming, flying)     |
| F                  | Move Down (ladders, swimming, flying)   |
| E                  | Run                                     |
| I                  | Enter inventory                         |
| Q                  | Examine pointed block                   |
| H                  | Use item in hand                        |
| T                  | Talk / commands                         |
| K                  | Toggle fly mode                         |
| F1                 | Hide / show hud                         |
| F2                 | Hide / show chat                        |
| F3                 | Disable / enable fog                    |
| F4                 | Disable / enable camera update          |
| F5                 | Toggle through debug information        |
| F6                 | Toggle through profiler                 |
| F12                | Take a screenshot                       |

# License

**Code:**

Voxelands is licensed under the GPLv3 or later.

Copyright (c) 2022-2022 Mikita Wisniewski <rudzik8@protonmail.com>

Copyright (c) 2013-2016 Lisa 'darkrose' Milne <lisa@ltmnet.com>

Forked from Minetest 0.3.x

Copyright (c) 2010-2011 Perttu Ahola <celeron55@gmail.com>

See commits history for all contributors.

**Textures, Sounds, and Models:**

This does not apply to packs made by others.

Attribution-ShareAlike 3.0 Unported (CC BY-SA 3.0)
http://creativecommons.org/licenses/by-sa/3.0/

To satisfy the attribution clause, any project using Voxelands' assets must provide a prominent notice as part of each of said project's credits notices (such as in documentation, on a website, and/or any other credits screen associated with the project) showing both the CC-BY-SA licensing, the ownership by Voxelands of the asset, and a link to the Voxelands' project at https://git.minetest.land/oerkki/voxelands/.

**Artists include:**

* sdzen
* darkrose
* sapier
* Tom Peter
* Telaron
* juskiddink

With special thanks to http://www.opengameart.org/

# Development

Voxelands is under active development, the latest updates can be found at https://git.minetest.land/oerkki/voxelands
