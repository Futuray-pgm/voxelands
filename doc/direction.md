# Voxelands Direction Document

Styling based on [Minetest Direction Document](https://github.com/minetest/minetest/blob/master/doc/direction.md).

## 1. Long-term Roadmap

Currently not much was done to the game and LTR can be changed in
future. But still, our long-term roadmap currently contains 3 things:

1. Use more of Minetest things to add solid backing (less to maintain)
2. Be a complete voxel sandbox game working OOTB (just like MC)
3. Be more voxel than cubic (more meshes and nodeboxes)

## 2. Medium-term Roadmap

The medium-term roadmap currently contains 5 things to add/change:

1. Modding (implement as Game+Mods (MC-alike), not Game=Mods (MT-alike))
2. Packs (texturepacks, soundpacks, modelpacks, resourcepacks) (#14)
3. Graphics (fix weird glitches and switch to IrrlichtMT) (#2)
4. Workflow (add more people to the development, automatize releasing)
5. Site (online page for Voxelands based on the voxelands-www repo)

## 3. Short-term Roadmap

The short-term roadmap is temporary, it's here only while critical bugs
(like crashes) aren't fixed.

We can count these things as a short-term roadmap:

1. Fix everything crashing (#1)
2. Convert most of the docs from plaintext to markdown
3. Do something with changelogs

After these are done, this section won't be here anymore and we'll count
(bug/crash)-labeled issues as short-term by default.
